/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    EnhancedLinkedBag.cpp
 * @authors Dillon Flohr <dillon987@live.missouristate.edu>
 *          Jake Wakefield <wakefield515@live.missouristate.edu>
 * @brief   Implementation file for an Enhanced Linked Bag
 *
 */
#include "EnhancedLinkedBag.h"

template<class ItemType>
EnhancedLinkedBag<ItemType> EnhancedLinkedBag<ItemType>::unionWithBag(const EnhancedLinkedBag<ItemType>& other) const {
	
	EnhancedLinkedBag<ItemType> unionBag;
	
	std::vector<ItemType> bag1 = this -> toVector();
	std::vector<ItemType> bag2 = other.toVector();
	for (ItemType element : bag1) {
		unionBag.add(element);
	}
	for (ItemType element : bag2) {
		unionBag.add(element);
	}
	return unionBag;
}

template<class ItemType>
EnhancedLinkedBag<ItemType> EnhancedLinkedBag<ItemType>::intersectionWithBag(const EnhancedLinkedBag<ItemType>& other) const {
	
	EnhancedLinkedBag<ItemType> intersectBag;
	
	std::vector<ItemType> bag1 = this -> toVector();
	std::vector<ItemType> bag2 = other.toVector();
	for (ItemType element : bag1) {
		if (other.contains(element)) {
			intersectBag.add(element);
		}
	}
	return intersectBag;
}

template<class ItemType>
EnhancedLinkedBag<ItemType> EnhancedLinkedBag<ItemType>::differenceWithBag(const EnhancedLinkedBag<ItemType>& other) const {
	
	EnhancedLinkedBag<ItemType> differenceBag;
	
	std::vector<ItemType> bag1 = this -> toVector();
	std::vector<ItemType> bag2 = other.toVector();
	for(ItemType element : bag1) {
		if (other.getFrequencyOf(element) == 0) {
			differenceBag.add(element);
		}
	}
	return differenceBag;
}