/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    EnhancedLinkedBag.h
 * @authors Dillon Flohr <dillon987@live.missouristate.edu>
 *          Jake Wakefield <wakefield515@live.missouristate.edu>
 * @brief   Header file for an Enhanced Linked Bag.
 *
 */
#ifndef ENHANCED_LINKED_BAG_
#define ENHANCED_LINKED_BAG_

#include "LinkedBag.h"

template<class ItemType>
class EnhancedLinkedBag : public LinkedBag<ItemType> {
	
public:
	
	/** 
    * Make a union set of two bags.
	*
    * @param other Bag to perform the union with.
    * @return An EnhancedLinkedBag that is the union of this bag and other.
    */
	EnhancedLinkedBag<ItemType> unionWithBag(const EnhancedLinkedBag<ItemType>& other) const;
	/** 
    * Make a intersection set of two bags.
	*
    * @param other Bag to perform the intersection with.
    * @return An EnhancedLinkedBag that is the intersection of this bag and other.
    */
	EnhancedLinkedBag<ItemType> intersectionWithBag(const EnhancedLinkedBag<ItemType>& other) const;
	/** 
    * Make a difference set of two bags.
	*
    * @param other Bag to perform the difference with.
    * @return An EnhancedLinkedBag that is the difference of this bag and other.
    */
	EnhancedLinkedBag<ItemType> differenceWithBag(const EnhancedLinkedBag<ItemType>& other) const;
};

#include "EnhancedLinkedBag.cpp"
#endif